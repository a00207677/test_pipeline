package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import Computer_Repair_Shop.Computer_Repair_Shop.controller.UserDAO;
import Computer_Repair_Shop.Computer_Repair_Shop.controller.UtilsDAO;
import Computer_Repair_Shop.Computer_Repair_Shop.model.User;
import Computer_Repair_Shop.Computer_Repair_Shop.rest.JaxRsActivator;
import Computer_Repair_Shop.Computer_Repair_Shop.service.UserWS;

@RunWith(Arquillian.class)
public class UserWSAndDAOTest {
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test1.jar")
				.addClasses(User.class, UserWS.class, UserDAO.class, UtilsDAO.class, JaxRsActivator.class)
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	 
	@EJB
	private UserWS userWS;
	
	@EJB
	private UserDAO userDAO;
	
	@EJB
	private UtilsDAO utilsDAO;
	
	@Before
	public void setUp() {
		utilsDAO.deleteUserTable();
		userDAO.saveUser(new User("test_user01", 1, "password", "test@mail.com"));
		userDAO.saveUser(new User("test_user02", 2, "password", "test@mail.com"));
	}
	
	@Test
	public void testGetAllUsers() {
		List<User> users = userDAO.getAllUsers();
		assertEquals(users.size(), 2);
		assertEquals(userWS.findAllUsers().getStatus(), 200);
	}
	
	@Test
	public void testGetUserById() {
		User user = userDAO.getUserById(101);
		assertEquals(user.getUser_type(), 1);
		assertEquals(user.getEmail(), "test@mail.com");
		assertEquals(user.getUsername(), "test_user01");
		assertEquals(userWS.getUserById(101).getStatus(), 200);
	}
	
	@Test
	public void testGetUserByUserName() {
		User user = userDAO.getUserByName("test_user02").get(0);
		assertEquals(user.getUser_type(), 2);
		assertEquals(user.getEmail(), "test@mail.com");
		assertEquals(user.getUsername(), "test_user02");
		assertEquals(userWS.getUserByName("test_user02").getStatus(), 200);
	}
	
	@Test
	public void testCreateUser() {
		User user = new User();
		user.setEmail("test_created_user@mail.com");
		user.setPassword("password");
		user.setUser_type(1);
		user.setUsername("test_created_user");
		
		assertEquals(userWS.saveData(user).getStatus(), 201);
		
		assertEquals(3, userDAO.getAllUsers().size());
	}
	
	@After
	public void repopulateUserTable() {
		
	}
}
