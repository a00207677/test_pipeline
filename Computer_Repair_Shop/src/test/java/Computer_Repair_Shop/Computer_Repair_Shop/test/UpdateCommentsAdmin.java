package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UpdateCommentsAdmin {
	private WebDriver webDriver = Hooks.driver;

	@Given("^I am logged in and on the admin dashboard to update comments$")
	public void adminDashboard() throws InterruptedException {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/index.html");
		Thread.sleep(2000);
	}

	@When("^I change the ticket comments to (.*)$")
	public void changeComments(String comments) throws InterruptedException {
		webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[2]/div/table/tbody/tr[2]/td[3]/div/textarea")).clear();
		webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[2]/div/table/tbody/tr[2]/td[3]/div/textarea")).sendKeys(comments);
		Thread.sleep(1000);
		webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[2]/div/table/tbody/tr[2]/td[7]/button[1]")).click();
	}

	@And("^the page reloads and refreshes comments$")
	public void reloadPage() {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/index.html");
	}

	@Then("^I validate that the comments are now changed to (.*)$")
	public void confirmUpdate(String newComments) throws InterruptedException {
		Thread.sleep(2000);
		String commentsOnPage = webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[2]/div/table/tbody/tr[2]/td[3]/div/textarea")).getText();
		assertEquals(commentsOnPage, newComments);
	}
}
