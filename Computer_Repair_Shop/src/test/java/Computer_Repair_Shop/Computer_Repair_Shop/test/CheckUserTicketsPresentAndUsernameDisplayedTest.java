package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CheckUserTicketsPresentAndUsernameDisplayedTest {
	private WebDriver webDriver = Hooks.driver;
	
	@Given("^I am logged in as (.*)$")
	public void userIsOnLoggedIn(String username) throws Throwable {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/dashboard_customer.html");
		Thread.sleep(3000);
		assertEquals(username, "customertest2");
	}
	
	@When("^my tickets are displayed$")
	public void ticketsDisplayed() throws InterruptedException {
		Thread.sleep(1000);
		String tableContent = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/main/div/div[2]/div/div/table/tbody/tr[1]/td[1]")).getText();
		assertEquals(tableContent, "202");
	}
	
	@And("^when I check ticket number (.*)$")
	public void enterUserName(String ticketID) {
		String ticketIDFromPage = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/main/div/div[2]/div/div/table/tbody/tr[1]/td[1]")).getText();
		assertEquals(ticketIDFromPage, ticketID);
	}
	
	@Then("^I verify the ticket has its current (.*)$")
	public void userLoggedIn(String status) {
		//Thread.sleep(1000);
		String statusFromPage = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/main/div/div[2]/div/div/table/tbody/tr[1]/td[5]")).getText();
		assertEquals(statusFromPage, status);
	}
}
