package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RegisterUserTest {
	private WebDriver webDriver = Hooks.driver;
	
	@Given("^The user is on the register.html page$")
	public void user_is_on_register_page() {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/register.html");
	}
	
	@When("^the user enters their username as (.*)$")
	public void user_enters_username(String username) {
		webDriver.findElement(By.id("username")).sendKeys(username);
	}
	
	@And("^enters their password as (.*)$")
	public void user_enters_password(String password) {
		webDriver.findElement(By.id("password")).sendKeys(password);
	}
	
	@And("^enters their email as (.*)$")
	public void user_enters_email(String email) {
		webDriver.findElement(By.id("email")).sendKeys(email);
	}
	
	@And("^selects their user type as (.*)$")
	public void user_enters_usertype(String user_type) {
		webDriver.findElement(By.id("user_type")).sendKeys(user_type);
	}
	
	@And("^clicks on the submit button$")
	public void clicks_submit() {
		WebElement buttonElement = webDriver.findElement(By.xpath("/html/body/div/div[1]/main/div/div/div/div/div[2]/button"));
		buttonElement.click();
	}
	
	@Then("^a message will be displayed (.*)$")
	public void message_dsiplayed(String msg) {
		String message = webDriver.findElement(By.xpath("/html/body/div/div[1]/main/div/div/div/div/div[3]")).getText();
		assertEquals(message, msg);
	}
}
