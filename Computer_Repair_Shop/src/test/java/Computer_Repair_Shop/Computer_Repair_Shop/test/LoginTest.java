package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginTest {
	private WebDriver webDriver = Hooks.driver;
	
	@Given("^The user is on the login page$")
	public void userIsOnLogInPage() throws Throwable {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/login.html");
	}
	
	@When("^the user enters their password as (.*)")
	public void enterPassWord(String password) {
		webDriver.findElement(By.id("password")).sendKeys(password);
	}
	
	@And("^enters the username as (.*)$")
	public void enterUserName(String username) {
		webDriver.findElement(By.id("username")).sendKeys(username);
	}
	
	@And("^the login button is clicked$")
	public void clickLoginButton() {
		WebElement buttonElement = webDriver.findElement(By.xpath("/html/body/div/div[1]/main/div/div/div/div/div[2]/button"));
		buttonElement.click();
	}
	
	@Then("^the user is logged in and a message is displayed containing (.*)$")
	public void userLoggedIn(String username) throws InterruptedException {
		Thread.sleep(1000);
		String message = webDriver.findElement(By.id("alertMessage")).getText();
		assertTrue(message.contains("Success! : " + username + " logged in to Fix-IT"));
	}
}
