package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CustomerAddTicket {
private WebDriver webDriver = Hooks.driver;
	
	@Given("^I am on the customer dashboard page$")
	public void userIsOnDashboardPage() throws Throwable {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/dashboard_customer.html");
	}
	
	@When("^I click on create ticket button$")
	public void clickCreateTicket() throws InterruptedException {
		Thread.sleep(3000);
		WebElement createTicketButton = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/main/div/div[1]/button"));
		createTicketButton.click();
	}
	
	@When("^I select (.*)$")
	public void selectType(String type) throws InterruptedException {
		Thread.sleep(3000);
		Select options = new Select(webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[2]/select")));
		options.selectByVisibleText(type);
	}
	
	@And("^I enter a valid comment (.*)$")
	public void enterComment(String comment) {
		webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[3]/input")).sendKeys(comment);
	}
	
	@And("^I click create buttom$")
	public void clickCreateButton() {
		WebElement createButton = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[4]/button[1]"));
		createButton.click();
	}
	
	@Then("^a message should be displayed to indicate the ticket is created -> (.*)$")
	public void displayMessage(String message) throws InterruptedException {
		Thread.sleep(1000);
		String messageFromHtml = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div")).getText();
		assertEquals(messageFromHtml, message);
	}
}
