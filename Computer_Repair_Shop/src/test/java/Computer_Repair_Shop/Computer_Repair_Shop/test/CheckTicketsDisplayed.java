package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class CheckTicketsDisplayed {
	
	private WebDriver webDriver = Hooks.driver;
	
	@Given("^The user is logged in$")
	public void userIsLoggedIn() throws Throwable {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/index.html");
	}
	
	@Then("^Display the tickets in a datatable$")
	public void displayTicketsInDataTable() throws Throwable {
		Thread.sleep(3000);
		System.out.println("displayTickets");
		WebElement element = webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[3]/div[1]/div"));
		String message = element.getText();
		assertTrue(message.contains("Showing 1 to 7 of 7 entries"));
	}

}
