package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import Computer_Repair_Shop.Computer_Repair_Shop.controller.TicketDAO;
import Computer_Repair_Shop.Computer_Repair_Shop.controller.UtilsDAO;
import Computer_Repair_Shop.Computer_Repair_Shop.model.Ticket;
import Computer_Repair_Shop.Computer_Repair_Shop.rest.JaxRsActivator;
import Computer_Repair_Shop.Computer_Repair_Shop.service.TicketWS;


//	@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Arquillian.class)
public class TicketDAOAndWSTest {
	
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test1.jar")
				.addClasses(Ticket.class, TicketWS.class, TicketDAO.class, UtilsDAO.class, JaxRsActivator.class)
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	 
	@EJB
	private TicketWS ticketWS;
	
	@EJB
	private TicketDAO ticketDAO;
	
	@EJB
	private UtilsDAO utilsDAO;
	
	@Before
	public void setUp() {
		utilsDAO.deleteTable();
		ticketDAO.createTicket(new Ticket(101, "Console", "PS4 Controller charging port issue."));
		ticketDAO.createTicket(new Ticket(102, "Phone", "iPhone X battery replacement"));
		ticketDAO.createTicket(new Ticket(101, "Laptop/Computer", "120GB SSD installation."));
	}
	
	@Test
	public void testGetAllTickets() {
		List<Ticket> ticketList = ticketDAO.getAllTickets();
		assertEquals(ticketList.size(), 3);
		assertEquals(ticketWS.getAllTickets().getStatus(), 200);
	}
	
	@Test
	public void testGetTicketById() {
		List<Ticket> ticketList = ticketDAO.getAllTickets();
		Ticket ticketToTest = ticketList.get(0);
		Ticket ticketById = ticketDAO.getTicketByID(200);
		assertTrue(ticketToTest.getTicketID() == ticketById.getTicketID());
		assertEquals(ticketWS.getTicketByID(200).getStatus(), 201);
	}
	
	@Test
	public void testTicketIdNotFound() {
		assertEquals(ticketWS.getTicketByID(999).getStatus(), 201);
	}
	
	@Test
	public void testCancelTicket() {
		List<Ticket> ticketsBeforeDelete = ticketDAO.getAllTickets();
		assertEquals(ticketsBeforeDelete.size(), 3);
		
		ticketDAO.cancelTicket(200);

		List<Ticket> ticketsAfterDelete = ticketDAO.getAllTickets();
		assertEquals(ticketsAfterDelete.size(), 2);
		
		assertEquals(ticketWS.cancelTicket(201).getStatus(), 204);
		
	}
	
	@Test
	public void testCannotDeleteTicketIfNotStatusScheduled() {
		Ticket ticket = new Ticket(101, null, null);
		ticket.setComments("test comments");
		ticket.setRepairType("Phone");
		ticket.setStatus("In Progress");
		ticketDAO.createTicket(ticket);
		
		assertEquals(ticketWS.cancelTicket(203).getStatus(), 400);
	}
	
	@Test
	public void testUpdateTicket() {
		Ticket ticketsBeforeUpdate = ticketDAO.getAllTickets().get(0);
		assertEquals(ticketsBeforeUpdate.getTicketID(), 200);
		assertEquals(ticketsBeforeUpdate.getComments(), "PS4 Controller charging port issue.");
		
		ticketsBeforeUpdate.setComments("Test Update");
		ticketDAO.updateTicket(ticketsBeforeUpdate);
		assertEquals(ticketWS.updateTicket(ticketsBeforeUpdate).getStatus(), 200);
		
		Ticket ticketsAfterUpdate = ticketDAO.getAllTickets().get(0);
		assertEquals(ticketsAfterUpdate.getTicketID(), 200);
		assertEquals(ticketsAfterUpdate.getComments(), "Test Update");
		
	}
	
	@Test
	public void testCreateNewTicket() {
		Ticket ticket = new Ticket(101, null, null);
		ticket.setComments("test comments");
		ticket.setRepairType("Phone");
		ticket.setStatus("In Progress");
		
		assertEquals(ticketWS.saveData(ticket).getStatus(), 201);

		ticket = ticketDAO.getTicketByID(203);
		assertEquals(ticket.getComments(), "test comments");
		assertEquals(ticket.getRepairType(), "Phone");
		assertEquals(ticket.getStatus(), "In Progress");
		assertEquals(ticket.getCustomerID(), 101);
		
		assertEquals(ticketDAO.getAllTickets().size(), 4);
	}
	
	@After
	public void repopulateTicketTable() {
		
	}
			
}
