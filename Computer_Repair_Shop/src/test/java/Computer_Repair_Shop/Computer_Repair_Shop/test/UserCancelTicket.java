package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UserCancelTicket {
	private WebDriver webDriver = Hooks.driver;
	
	@Given("^The user is on the customer dashboard$")
	public void userIsOnCustDashboard() throws Throwable {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/dashboard_customer.html");
	}
	
	//I click on cancel for the ticket with a status of 
	@When("^I click on cancel for the ticket with a status of (.*)$")
	public void cancelTicket(String status) throws InterruptedException {
		Thread.sleep(1000);
		String scheduledTicket = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/main/div/div[2]/div/div/table/tbody/tr[3]/td[5]")).getText();
		assertEquals(scheduledTicket, status);
		WebElement cancelButton = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/main/div/div[2]/div/div/table/tbody/tr[3]/td[6]/button"));
		cancelButton.click();
	}
	
	//I should see the following message -> <message>
	@Then("^I should see the following message -> (.*)$")
	public void displayMessage(String message) throws InterruptedException {
		Thread.sleep(1000);
		String messageFromPage = webDriver.findElement(By.xpath("/html/body/div[2]/div[2]/main/div/div[2]/div/div/div")).getText();
		assertTrue(messageFromPage.contains(message));
	}
}
