package Computer_Repair_Shop.Computer_Repair_Shop.test;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UpdateStatusAdmin {
	private WebDriver webDriver = Hooks.driver;

	@Given("^I am logged in and on the admin dashboard$")
	public void adminDashboard() throws InterruptedException {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/index.html");
		Thread.sleep(2000);
		String statusSched = webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[4]")).getText();
		assertEquals(statusSched, "In Progress");
	}

	@When("^I click on the Ready For Collection button$")
	public void clickButton() {
		WebElement inProgButton = webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[6]/button[3]"));
		inProgButton.click();
	}

	@And("^the page reloads$")
	public void reloadPage() {
		webDriver.get("http://localhost:8080/Computer_Repair_Shop/index.html");
	}

	@Then("^the status should be (.*)$")
	public void checkStatus(String status) throws InterruptedException {
		Thread.sleep(2000);
		String statusRFC = webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[6]/button[3]")).getText();
		assertEquals(statusRFC, status);
	}

	@Then("^I validate that the comments are now (.*)$")
	public void validateComments(String comments) {
		String commentsOnPage = webDriver.findElement(By.xpath("/html/body/div/div[3]/main/div/div[3]/div[2]/div/div/div[2]/div/table/tbody/tr[1]/td[3]/div/textarea")).getText();
		assertEquals(commentsOnPage, comments);
	}
}