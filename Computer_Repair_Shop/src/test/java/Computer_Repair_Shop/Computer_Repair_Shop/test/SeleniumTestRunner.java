package Computer_Repair_Shop.Computer_Repair_Shop.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/CucumberFeatures")
public class SeleniumTestRunner {

}
