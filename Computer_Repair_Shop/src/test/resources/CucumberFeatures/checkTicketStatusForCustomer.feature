@browser
Feature: As a customer I want to be able to view my jobs progress.  

	Background: 
		Given The user is on the login page
    When the user enters their password as password
    And enters the username as customertest2
    And the login button is clicked
    Then the user is logged in and a message is displayed containing customertest2

 

  Scenario Outline: Customer checks ticket status
    Given I am logged in as <username>
    When my tickets are displayed
    And when I check ticket number <ticketID>
    Then I verify the ticket has its current <status>

    Examples: 
      | username    	| ticketID | status    |
      | customertest2 | 202			 | In Progress |
