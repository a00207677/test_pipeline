@browser
Feature: As a customer I want to be able to cancel jobs I have booked in if they have not been marked as in progress by the IT Technicians. 

	Background: 
		Given The user is on the login page
    When the user enters their password as password
    And enters the username as customertest2
    And the login button is clicked
    Then the user is logged in and a message is displayed containing customertest2

  Scenario Outline: The user can cancel jobs
    Given The user is on the customer dashboard
    When I click on cancel for the ticket with a status of <status>
    Then I should see the following message -> <message>

    Examples: 
      | status  | message |
      | Scheduled | cancelled successfully! | 
