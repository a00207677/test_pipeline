@browser
Feature: As a customer/admin I want to be able to login to the system and view jobs

 Scenario Outline: The User can login to the system and view jobs
    Given The user is on the login page
    When the user enters their password as <password>
    And enters the username as <username>
    And the login button is clicked
    Then the user is logged in and a message is displayed containing <username>

	Examples: 
     | password 		 | username  		|
     | password 		 | admintest  | 
     | password 	   | customertest2	| 
