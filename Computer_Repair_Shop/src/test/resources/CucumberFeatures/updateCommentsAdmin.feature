@browser
Feature: As an IT Technician I want the ability to comment on jobs so to update my fellow colleagues on work.  

	Background: 
		Given The user is on the login page
    When the user enters their password as password
    And enters the username as admintest
    And the login button is clicked

  Scenario Outline: Admin updating job comments
    Given I am logged in and on the admin dashboard to update comments
    When I change the ticket comments to <comments>
    And the page reloads and refreshes comments
    Then I confirm that the comments are now changed to <comments>


    Examples: 
      | comments | 
      | TESTCOMMENTS | 
