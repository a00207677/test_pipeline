var username = "";

var findAllForCustomer=function() {
	
	var rootURLCustomer = "http://localhost:8080/Computer_Repair_Shop/rest/user";
	var ticketGetURL = "http://localhost:8080/Computer_Repair_Shop/rest/tickets/";
	
	console.log('findAll');
	console.log('user id logged in: '+ localStorage.getItem("userIDLoggedIn"));
	$.ajax({
		type: 'GET',
		url: rootURLCustomer + '/' + localStorage.getItem("userIDLoggedIn"),
		dataType: "json", // data type of response
		success: function(data) {
			var user = data;
			$('#ticketUsername').val(user.username);
			console.log('USER = ' + JSON.stringify(user));

			$.ajax({
				type : 'GET',
				url : ticketGetURL,
				contentType : "application/json",
				success : function(data2) {
					var list2 = data2;
					$.each(list2, function(index2, ticket) {
						if(ticket.customerID == user.id){
							console.log('Ticket ' + JSON.stringify(ticket));
							if(ticket.status == 'Scheduled'){
								$('#table_content_customer').append('<tr>'
										+ '<td>' + ticket.ticketID + '</td>'
										+ '<td>' + ticket.customerID + '</td>'
										+ '<td>' + ticket.repairType + '</td>'
										+ '<td>' + ticket.comments + '</td>'
										+ '<td>' + ticket.status + '</td>'
										+ '<td>' + '<button type="button" class="btn btn-success" onclick="cancel('+ticket.ticketID+')">Cancel</button>' + '</td>'
										+ '</tr>');
							}else{
								$('#table_content_customer').append('<tr>'
										+ '<td>' + ticket.ticketID + '</td>'
										+ '<td>' + ticket.customerID + '</td>'
										+ '<td>' + ticket.repairType + '</td>'
										+ '<td>' + ticket.comments + '</td>'
										+ '<td>' + ticket.status + '</td>'
										+ '<td>' + '<button type="button" class="btn btn-danger" onclick="cancel('+ticket.ticketID+')">Cancel</button>' + '</td>'
										+ '</tr>');
							}
						}
							
					});
					$("#user_name").text(localStorage.getItem("userNameLoggedIn"));
				},
				error : function(response, textStatus, jqHXR) {
					console.log('Error' + response);
				}
			})
			
		}
	});
};

var createTicket = function(){
	
	var ticketURL = "http://localhost:8080/Computer_Repair_Shop/rest/tickets/";
	var userURL = "http://localhost:8080/Computer_Repair_Shop/rest/user/search/";
	
	var ticket = {
		customerID: $('#ticketUsername').val(),
		repairType: $('#ticketRepairType').val(),
		comments: $('#ticketComments').val(),
		status: 'Scheduled'
	};
	
	console.log(JSON.stringify(ticket));
	
	var userIDForTicket = '';
	var userNameForTicket = ticket.customerID;
	
	
	console.log('getUserIDForTicket: ' + userNameForTicket);
	$.ajax({
		type : 'GET',
		url : userURL + ticket.customerID,
		contentType : "application/json",
		success : function(data) {
			var list = data;
			$.each(list, function(index, user) {
				//this.userIDForTicket = user.id;
				console.log('CustomerID: ' + user.id);
				console.log(JSON.stringify(user));
				ticket.customerID = user.id;
				console.log('TICKET: ' + ticket.customerID);
				
				$.ajax({
					beforeSend: function(){
						console.log('BeforeSend: ' + ticket.customerID);
					},
					type : 'POST',
					url : ticketURL,
					data : JSON.stringify(ticket),
					contentType : "application/json",
					success : function(response, textStatus, jqXHR) {
						$('#createTicketMessage').html('<div class="alert alert-primary" id="updated_message" role="alert">Ticket created successfully!</div>');
						setTimeout(function(){
							location.reload(true);
						}, 2000);
					},
					error : function(response, textStatus, jqHXR) {
						console.log(JSON.stringify(ticket));
					}
				})
				
			});
		},
		error : function(response, textStatus, jqHXR) {
			console.log(JSON.stringify(ticket));
		}
	})
};

var cancel = function(id){
	var cancelURL = "http://localhost:8080/Computer_Repair_Shop/rest/tickets/";
	
	$.ajax({
		type: 'DELETE',
		url: cancelURL + id,
		success: function(){
			console.log('Success');
			$('#update_cancel_msg').html('<div class="alert alert-danger" role="alert">Ticket number ' + id + ' cancelled successfully!</div>');
			setTimeout(function(){
				location.reload(true);
			}, 2000);},
		
		error: function(response, textStatus, jqHXR) {
			setTimeout(function(){
				window.location.replace("http://localhost:8080/Computer_Repair_Shop/401.html");
			 },3000);
		}
	});
};

$(document).ready(function(){
	findAllForCustomer();
});