
function user(id, username, user_type, email, password) {
	id = id;
	username = username;
	user_type = user_type;
	email = email;
	password = password;
}

var saveUser = function(){
	var URL = "http://localhost:8080/Computer_Repair_Shop/rest/user";

	if ($('#username').val() == ""){
		$("#updateMessage").html('<div class="alert alert-danger" role="alert">'
				+ 'Error : invalid username'
				+ '</div>'	);
	} else  if ($('#password').val() == ""){
		$("#updateMessage").html('<div class="alert alert-danger" role="alert">'
				+ 'Error : invalid password'
				+ '</div>'	);
	} else if ($('#email').val() == "" || !$('#email').val().includes("@") ){
		$("#updateMessage").html('<div class="alert alert-danger" role="alert">'
				+ 'Error : invalid email'
				+ '</div>'	);
	} else {
		var user = {
			username : $('#username').val(),
			user_type : $('#user_type').val(),
			password : $('#password').val(),
			email : $('#email').val()
			
		};

		console.log('saveUser');
		$.ajax({
			type : 'POST',
			url : URL,
			data : JSON.stringify(user),
			contentType : "application/json",
			success : function(response, textStatus, jqXHR) {
				//$("#status_msg_user").text(jqXHR.responseText);
				
				clearUserEntries();
				$("#updateMessage").html('<div class="alert alert-primary" role="alert">User created successfully! Redirecting to login...</div>');
				setTimeout(function(){
					//window.location.replace("http://localhost:8080/Computer_Repair_Shop/login.html");
				 },3000);
			},
			error : function(response, textStatus, jqHXR) {
				console.log(user);
			}
		})
	}
}


function login(){
	var URL = "http://localhost:8080/Computer_Repair_Shop/rest/user/search/";
	var user_username_login = $('#username').val();
	var user_password_login = $('#password').val();
	
	console.log(user_username_login);
	
	$.ajax({
		type: 'GET',
		url: URL + user_username_login,
		dataType: "json", // data type of response
		success: function(data){
			var list = data;
			checkLoginCredentials(list, user_username_login, user_password_login);
		}
	
	});
}

var checkLoginCredentials = function(list, user_username_login, user_password_login){
	$.each(list, function(index, user) {
		console.log(JSON.stringify(user));
		console.log("Username from html: " + user_username_login + "   Username from DB: " +user.username+ " Password from html" + user_password_login + "     Password from DB: " + user.password);
		if(user_username_login == user.username && user_password_login == user.password){
			localStorage.setItem("userNameLoggedIn", user.username);
			$("#alertMessage").html('<div class="alert alert-success" role="alert">'
			+ 'Success! : ' + user.username + ' logged in to Fix-IT'
			+ '</div>'	);
			//alert('Login successful!');
			console.log("Username from html: " + user_username_login + "   Username from DB: " +user.username+ " Password from html: " + user_password_login + "     Password from DB: " + user.password);
			if(user.user_type == 2){
				setTimeout(function(){window.location.replace("http://localhost:8080/Computer_Repair_Shop/index.html");}, 2000);
				localStorage.setItem("userIDLoggedIn", user.id);
			}else{
				//localStorage.setItem("userNameLoggedIn", user.username);
				setTimeout(function(){window.location.replace("http://localhost:8080/Computer_Repair_Shop/dashboard_customer.html");}, 2000);
				localStorage.setItem("userIDLoggedIn", user.id);
			}
			
		}else{
			$("#alertMessage").html('<div class="alert alert-danger" role="alert">'
			 + 'Error logging in: Username or Password incorrect!'
			 + '</div>');
			console.log('Fail');
		}
	});
}

var clearUserEntries = function() {
	$('#username').val('');
	$('#email').val('');
	$('#password').val('');
};

