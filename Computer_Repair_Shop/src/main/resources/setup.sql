use repairs;

DROP table IF EXISTS ticket;
CREATE TABLE ticket(
	ticketID int AUTO_INCREMENT PRIMARY KEY,
    customerID int,
    repairType VARCHAR(50),
    comments VARCHAR(250),
    status VARCHAR(40)
);
ALTER TABLE ticket AUTO_INCREMENT=201;

INSERT INTO ticket VALUES(null, 101, 'Laptop/Desktop', 'Diagnose Dell PC. Running slow', 'Scheduled');
INSERT INTO ticket VALUES(null, 102, 'Console', 'Recondition Fan', 'In Progress');
INSERT INTO ticket VALUES(null, 102, 'Phone', 'Water Damage', 'In Progress');
INSERT INTO ticket VALUES(null, 104, 'Console', 'Charging Port on Controller', 'Scheduled');

DROP TABLE IF EXISTS user;
CREATE TABLE user(
	id INT auto_increment,
	username VARCHAR(40),
    user_type INT,
    password VARCHAR(100),
    email VARCHAR(50),
    date_created DATETIME,
    PRIMARY KEY (`id`)
);
ALTER TABLE user AUTO_INCREMENT=101;

INSERT INTO user VALUES(null, 'customertest', 1, 'password', 'test@test', NOW());
INSERT INTO user VALUES(null, 'customertest2', 1, 'password', 'test@test', NOW());
INSERT INTO user VALUES(null, 'admintest', 2, 'password', 'test@test', NOW());
INSERT INTO user VALUES(null, 'customertest3', 1, 'password', 'test@test', NOW());
