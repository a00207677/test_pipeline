package Computer_Repair_Shop.Computer_Repair_Shop.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import Computer_Repair_Shop.Computer_Repair_Shop.model.Ticket;
import Computer_Repair_Shop.Computer_Repair_Shop.controller.TicketDAO;

@Path("/tickets")
@Stateless
@LocalBean
public class TicketWS {

	@EJB
	private TicketDAO ticketDAO;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getAllTickets() {
		List<Ticket> tickets = ticketDAO.getAllTickets();
		return Response.status(200).entity(tickets).build();
	}
	
	@GET @Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getTicketByID(@PathParam("id") int id) {
		try {
			Ticket ticket = ticketDAO.getTicketByID(id);
			return Response.status(201).entity(ticket).build();
		}catch(Exception e) {

			return Response.status(407).entity("No Good").build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	public Response cancelTicket(@PathParam("id") int id) {
		Ticket ticket = ticketDAO.getTicketByID(id);
		
		if(ticket.getStatus().equals("Scheduled")) {
			ticketDAO.cancelTicket(ticket.getTicketID());
			return Response.status(204).build();
		}else {
			return Response.status(400).build();
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveData(Ticket ticket) {
		ticketDAO.createTicket(ticket);
		return Response.status(201).entity(ticket).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response updateTicket(Ticket ticket) {
		ticketDAO.updateTicket(ticket);
		return Response.status(200).entity(ticket).build();
	}
}
