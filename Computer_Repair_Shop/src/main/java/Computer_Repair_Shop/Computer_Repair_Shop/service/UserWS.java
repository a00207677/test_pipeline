package Computer_Repair_Shop.Computer_Repair_Shop.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import Computer_Repair_Shop.Computer_Repair_Shop.model.User;
import Computer_Repair_Shop.Computer_Repair_Shop.controller.UserDAO;

@Path("/user")
@Stateless
@LocalBean
public class UserWS {

	@EJB
	UserDAO userDAO;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllUsers() {
		List<User> user = userDAO.getAllUsers();
		return Response.status(200).entity(user).build();
	}

	
	 @GET
	 @Produces({MediaType.APPLICATION_JSON})
	 @Path("/{id}") 
	 public Response getUserById(@PathParam("id") int id) { 
		 User user = userDAO.getUserById(id);
		 return Response.status(200).entity(user).build(); 
	 }
	 

	@GET @Path("search/{username}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUserByName(@PathParam("username") String username) {
		List<User> user = userDAO.getUserByName(username);
		return Response.status(200).entity(user).build();
	}

	@POST
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response saveData(User user) {
		System.out.println("SaveData - WS");
		userDAO.saveUser(user);
		return Response.status(201).entity("User created successfully").build();
	}
}
