package Computer_Repair_Shop.Computer_Repair_Shop.controller;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Computer_Repair_Shop.Computer_Repair_Shop.model.User;

@Stateless
@LocalBean
public class UserDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void saveUser(User user) {
		System.out.println("SaveData - DAO");
		User usr = user;
		em.persist(usr);
	}
	
	public List<User> getAllUsers() {
		Query query = em.createQuery("SELECT u FROM User u");
		return query.getResultList();
	}
	
	public User getUserById(int id) {
		return em.find(User.class, id); 
	}
	
	public List<User> getUserByName(String username) {
		Query query = em.createQuery("Select u from User u Where u.username = ?1");
		query.setParameter(1, username);
		return query.getResultList();
	}
}
