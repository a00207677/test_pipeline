package Computer_Repair_Shop.Computer_Repair_Shop.controller;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@LocalBean
public class UtilsDAO {

    @PersistenceContext
    private EntityManager em;
    
	public void deleteTable(){
		em.createQuery("DELETE FROM Ticket").executeUpdate();
		em.createNativeQuery("ALTER TABLE ticket AUTO_INCREMENT=200")
		.executeUpdate();
		
	}
	
	public void deleteUserTable() {
		em.createQuery("DELETE FROM User").executeUpdate();
		em.createNativeQuery("ALTER TABLE user AUTO_INCREMENT=101")
		.executeUpdate();
	}
      
}
