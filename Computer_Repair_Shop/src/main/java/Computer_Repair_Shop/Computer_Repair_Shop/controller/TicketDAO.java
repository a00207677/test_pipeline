package Computer_Repair_Shop.Computer_Repair_Shop.controller;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Computer_Repair_Shop.Computer_Repair_Shop.model.Ticket;

@Stateless
@LocalBean
public class TicketDAO {

	@PersistenceContext
	private EntityManager em;
	
	public void createTicket(Ticket ticket) {
		em.persist(ticket);
	}

	public List<Ticket> getAllTickets() {
		Query query = em.createQuery("SELECT t FROM Ticket t");
		return query.getResultList();
	}
	
	public Ticket getTicketByID(int id) {
		return em.find(Ticket.class, id);
	}
	
	public void cancelTicket(int id) {
		em.remove(getTicketByID(id));
	}
	
	public void updateTicket(Ticket ticket) {
		em.merge(ticket);
	}

}
