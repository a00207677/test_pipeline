package Computer_Repair_Shop.Computer_Repair_Shop.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="ticket")
@XmlRootElement
public class Ticket implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ticketID;
	
	private int customerID;
	private String repairType;
	private String comments;
	private String status;
	
	public Ticket() {super();}
	
	public Ticket(int cust_id, String rep_type, String comments) {
		this.customerID = cust_id;
		this.repairType = rep_type;
		this.comments = comments;
		this.status = "Scheduled";
	}
	
	public int getTicketID() {
		return ticketID;
	}

	public int getCustomerID() {
		return customerID;
	}

	public String getRepairType() {
		return repairType;
	}
	public void setRepairType(String repairType) {
		this.repairType = repairType;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Ticket [ticketID=" + ticketID + ", customerID=" + customerID + ", repairType=" + repairType
				+ ", comments=" + comments + ", status=" + status + ", readyForCollection=" + "]";
	}
	
}
