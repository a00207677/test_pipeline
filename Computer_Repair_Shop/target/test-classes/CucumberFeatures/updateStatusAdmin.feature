@browser
Feature: As an IT Technician I want to be able to update job progress. 

	Background: 
		Given The user is on the login page
    When the user enters their password as password
    And enters the username as admintest
    And the login button is clicked

  Scenario Outline: Admin updating job progress
    Given I am logged in and on the admin dashboard
    When I click on the Ready For Collection button
    And the page reloads
    Then the status should be <status>
    Then I validate that the comments are now <comments>


    Examples: 
      | status  | comments | 
      | Ready For Collection |     PAID | 
