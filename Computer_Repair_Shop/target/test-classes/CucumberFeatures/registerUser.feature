@browser
Feature: As a customer I want to be able to create an account with the computer repair shop As an IT Technician I want to be able to create accounts on behalf of the customer.

  Scenario Outline: Register user from register.html page
    Given The user is on the register.html page
    When the user enters their username as <username>
    And enters their password as <password>
    And enters their email as <email>
    And selects their user type as <user_type>
    And clicks on the submit button
    Then a message will be displayed <message>

    Examples: 
      | username    | password | email         | user_type | message                                            |
      | test_user03 | password | test@mail.com | 1				 | User created successfully! Redirecting to login... |