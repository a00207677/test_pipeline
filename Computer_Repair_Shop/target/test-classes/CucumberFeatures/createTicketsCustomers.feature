@browser
Feature: As a customer I want to be able to book devices in for repair Computer Games Console etc 

	Background: 
		Given The user is on the login page
    When the user enters their password as password
    And enters the username as customertest2
    And the login button is clicked
    
  Scenario Outline: As a user I want to create tickets
    Given I am on the customer dashboard page
    When I click on create ticket button
    When I select <type>
    And I enter a valid comment <comment>
    And I click create buttom
    Then a message should be displayed to indicate the ticket is created -> <message>


    Examples: 
      | type  | comment 			| message  											|
      | Phone | Broken Screen | Ticket created successfully!  |
