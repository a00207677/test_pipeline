var rootURL="http://localhost:8080/Computer_Repair_Shop/rest/tickets"; 

var findAll=function() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
};

var renderList= function(data) {
    var list = data;
    $('#username_logged_in').text(localStorage.getItem("userNameLoggedIn"));
    console.log("UserName: " + localStorage.getItem("userNameLoggedIn"));
    $.each(list, function(index, ticket) {
    	if(ticket.comments == 'PAID'){
    		//document.getElementById("comments" + data.ticketID).disabled = true;
    		$('#table_data').append('<tr><td>'+ticket.ticketID+'</td><td>'
					+ ticket.customerID+'</td><td><div class="input-group"><textarea disabled="true" id="comments' + ticket.ticketID + '" class="form-control" aria-label="With textarea">'
					+ ticket.comments+'</textarea></div></td><td>'
					+ ticket.status+'</td><td>'
					+ ticket.repairType+
					
					'</td><td><button onclick="updateTicketScheduled(' 
						+ ticket.ticketID + ',' + ticket.customerID + ',\'' 
						+ ticket.comments +'\',\''+ ticket.repairType 
					+ '\')" type="button" id="updateStatusScheduledButton" class="btn btn-outline-primary">Scheduled</button><button type="button" id="updateStatusInProgressButton" onclick="updateTicketInProgress(' 
						+ ticket.ticketID + ',' + ticket.customerID + ',\'' 
						+ ticket.comments +'\',\''+ ticket.repairType 
					+ '\')" class="btn btn-outline-secondary">In Progress</button><button type="button" id="updateStatusResolvedButton" onclick="updateTicketResolved(' 
						+ ticket.ticketID + ',' + ticket.customerID + ',\'' 
						+ ticket.comments +'\',\''+ ticket.repairType 
					+ '\')" class="btn btn-outline-success">Ready For Collection</button></td><td><button type="button" onclick="updateTicketComments(' 
						+ ticket.ticketID + ')" class="btn btn-success"><i class="fas fa-check-circle"></i></button><button type="button" id="cancelButton" class="btn btn-danger"><i class="fas fa-undo"></i></button></td></tr>');
    		
    	}else{
			$('#table_data').append('<tr><td>'+ticket.ticketID+'</td><td>'
					+ ticket.customerID+'</td><td><div class="input-group"><textarea id="comments' + ticket.ticketID + '" class="form-control" aria-label="With textarea">'
					+ ticket.comments+'</textarea></div></td><td>'
					+ ticket.status+'</td><td>'
					+ ticket.repairType+
					
					'</td><td><button onclick="updateTicketScheduled(' 
						+ ticket.ticketID + ',' + ticket.customerID + ',\'' 
						+ ticket.comments +'\',\''+ ticket.repairType 
					+ '\')" type="button" id="updateStatusScheduledButton" class="btn btn-outline-primary">Scheduled</button><button type="button" id="updateStatusInProgressButton" onclick="updateTicketInProgress(' 
						+ ticket.ticketID + ',' + ticket.customerID + ',\'' 
						+ ticket.comments +'\',\''+ ticket.repairType 
					+ '\')" class="btn btn-outline-secondary">In Progress</button><button type="button" id="updateStatusResolvedButton" onclick="updateTicketResolved(' 
						+ ticket.ticketID + ',' + ticket.customerID + ',\'' 
						+ ticket.comments +'\',\''+ ticket.repairType 
					+ '\')" class="btn btn-outline-success">Ready For Collection</button></td><td><button type="button" onclick="updateTicketComments(' 
						+ ticket.ticketID + ')" class="btn btn-success"><i class="fas fa-check-circle"></i></button><button type="button" id="cancelButton" class="btn btn-danger"><i class="fas fa-undo"></i></button></td></tr>');
		}
    });
    
	$('#table_data').DataTable();
};

var updateTicketComments = function(id){
	var updateTicketURL = "http://localhost:8080/Computer_Repair_Shop/rest/tickets/";
	
	var commentsFromHTML = $('#comments' + id).val();
	console.log("CommentsFromHTML: " + commentsFromHTML);
	
	var ticket = {
		ticketID: '',
		customerID: '',
		repairType: '',
		comments: '',
		status: ''
	};
	
	$.ajax({
		type : 'GET',
		url : updateTicketURL + id,
		contentType : "application/json",
		success : function(data){
			
			console.log("DataTicket: " + JSON.stringify(data));
			
			ticket.ticketID = data.ticketID;
			ticket.customerID = data.customerID;
			ticket.repairType = data.repairType;
			ticket.status = data.status;
			ticket.comments = commentsFromHTML;
			
			console.log("TicketAfterUpdates: " + JSON.stringify(ticket));
			
			$.ajax({
				beforeSend: function(){
					console.log('Before Update Comments: ' + data.comments);
				},
				type : 'PUT',
				url : updateTicketURL + id,
				data : JSON.stringify(ticket),
				contentType : "application/json",
				success : function(){
					console.log("Ticket updated -> " + ticket.comments);
					$('#updateTicketStatus').html('<div class="alert alert-primary" role="alert">Comments updated!</div>');
					setTimeout(function(){
						location.reload(true);
					}, 2000);
				}
			});
		}
	});
}

var createTicket = function(){
	
	var ticketURL = "http://localhost:8080/Computer_Repair_Shop/rest/tickets/";
	var userURL = "http://localhost:8080/Computer_Repair_Shop/rest/user/search/";
	
	var ticket = {
		customerID: $('#ticketUsername').val(),
		repairType: $('#ticketRepairType').val(),
		comments: $('#ticketComments').val(),
		status: 'Scheduled'
	};
	
	console.log(JSON.stringify(ticket));
	
	var userIDForTicket = '';
	var userNameForTicket = ticket.customerID;
	
	
	console.log('getUserIDForTicket: ' + userNameForTicket);
	$.ajax({
		type : 'GET',
		url : userURL + ticket.customerID,
		contentType : "application/json",
		success : function(data) {
			var list = data;
			$.each(list, function(index, user) {
				//this.userIDForTicket = user.id;
				console.log('CustomerID: ' + user.id);
				console.log(JSON.stringify(user));
				ticket.customerID = user.id;
				console.log('TICKET: ' + ticket.customerID);
				
				$.ajax({
					beforeSend: function(){
						console.log('BeforeSend: ' + ticket.customerID);
					},
					type : 'POST',
					url : ticketURL,
					data : JSON.stringify(ticket),
					contentType : "application/json",
					success : function(response, textStatus, jqXHR) {
						$('#createTicketMessage').html('<div class="alert alert-success" role="alert">'
								+ 'Success : ticket created successfully'
								+ '</div>'	);
						setTimeout(function(){
							
							location.reload(true);
						}, 2000);
					},
					error : function(response, textStatus, jqHXR) {
						$('#createTicketMessage').html('<div class="alert alert-danger" role="alert">'
								+ 'Error : no such user'
								+ '</div>'	);
					}
				})
				
			});
		},
		error : function(response, textStatus, jqHXR) {
			console.log(JSON.stringify(ticket));
		}
	})
};


var updateTicketScheduled = function(ticketIDUpdate, customerIDUpdate, commentsUpdate, repairTypeUpdate) {
	var URL = "http://localhost:8080/Computer_Repair_Shop/ticket/";
	console.log('updateTicket');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + ticketIDUpdate,
		dataType: "json",
		data: JSON.stringify({
				ticketID: ticketIDUpdate,
				customerID: customerIDUpdate,
				comments: commentsUpdate,
				status: $('#updateStatusScheduledButton').text(),
				repairType: repairTypeUpdate
		}),
		success: function(data, textStatus, jqXHR){
			//alert('Ticket updated successfully');
			console.log('VAL: ' + $('#updateStatusScheduledButton').text());
			$('#updateTicketStatus').html('<div class="alert alert-primary" role="alert">Ticket updated successfully to ' + $('#updateStatusScheduledButton').text() + '!</div>');
			setTimeout(function(){
				location.reload(true);
			}, 2000);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('updateTicket error: ' + textStatus);
		}
	});
};

var updateTicketInProgress = function(ticketIDUpdate, customerIDUpdate, commentsUpdate, repairTypeUpdate) {
	var URL = "http://localhost:8080/Computer_Repair_Shop/ticket/";
	console.log('updateTicket');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + ticketIDUpdate,
		dataType: "json",
		data: JSON.stringify({
				ticketID: ticketIDUpdate,
				customerID: customerIDUpdate,
				comments: commentsUpdate,
				status: $('#updateStatusInProgressButton').text(),
				repairType: repairTypeUpdate
		}),
		success: function(data, textStatus, jqXHR){
			//alert('Ticket updated successfully');
			console.log('VAL: ' + $('#updateStatusInProgressButton').text());
			$('#updateTicketStatus').html('<div class="alert alert-primary" role="alert">Ticket updated successfully to ' + $('#updateStatusInProgressButton').text() + '!</div>');
			setTimeout(function(){
				location.reload(true);
			}, 2000);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('updateTicket error: ' + textStatus);
		}
	});
};

var updateTicketResolved = function(ticketIDUpdate, customerIDUpdate, commentsUpdate, repairTypeUpdate) {
	var URL = "http://localhost:8080/Computer_Repair_Shop/ticket/";
	console.log('updateTicket');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + ticketIDUpdate,
		dataType: "json",
		data: JSON.stringify({
				ticketID: ticketIDUpdate,
				customerID: customerIDUpdate,
				comments: 'PAID',
				status: $('#updateStatusResolvedButton').text(),
				repairType: repairTypeUpdate
		}),
		success: function(data, textStatus, jqXHR){
			//alert('Ticket updated successfully');
			console.log('VAL: ' + $('#updateStatusResolvedButton').text());
			$('#updateTicketStatus').html('<div class="alert alert-primary" role="alert">Ticket updated successfully to ' + $('#updateStatusResolvedButton').text() + '!</div>');
			setTimeout(function(){
				location.reload(true);
			}, 2000);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('updateTicket error: ' + textStatus);
		}
	});
};

$(document).ready(function(){
	findAll();
	
});