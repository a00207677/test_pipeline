var URL = "http://localhost:8080/Computer_Repair_Shop/rest/tickets";

function getHTTPObject() {
	var xhr = false;
	xhr = new XMLHttpRequest();
	return xhr;
}

function findAllTickets() {
	var request = getHTTPObject();
	if(request) {
		
		request.onreadystatechange = function() {
			renderList(request);
		};
		
		request.open("GET", URL, true);
		request.send(open);
	}
	
	return false;
}

function renderList(request){
	if(request.readyState == 4){
		if(request.status == 200 || request.status == 304){
			
			var data = JSON.parse(request.responseText);
			var table_content = "";
			var i;
			
			for(i = 0; i <data.length; i++){
				table_content += '<tr><td>' + data[i].ticketID + '</td>' +
							'<td>' + data[i].customerID + '</td>' +
							'<td>' + data[i].comments + '</td>' +
							'<td>' + data[i].status + '</td>' +
							'<td>' + data[i].repairType + '</td></tr>';
			}
			
			document.getElementById("ticket_data").innerHTML = table_content;
		}
	}
}

window.onload = function () {
	findAllTickets();
}