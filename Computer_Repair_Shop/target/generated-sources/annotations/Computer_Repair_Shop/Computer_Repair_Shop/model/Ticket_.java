package Computer_Repair_Shop.Computer_Repair_Shop.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Ticket.class)
public abstract class Ticket_ {

	public static volatile SingularAttribute<Ticket, String> comments;
	public static volatile SingularAttribute<Ticket, Integer> customerID;
	public static volatile SingularAttribute<Ticket, String> repairType;
	public static volatile SingularAttribute<Ticket, Integer> ticketID;
	public static volatile SingularAttribute<Ticket, String> status;

}

